#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/*
Program removes duplicate entries from arg1, and writes the result to arg2
TODO: Seems to delete last entry
TODO: Change newline tag (currently < )
*/

unsigned long long hash(unsigned char *str)
{
	unsigned long long hash = 5381;
	int c;
	while (c = *str++)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	return hash;
}

int main(int argc, char* argv[])
{
    if (argc == 1){
        printf("Usage: repeat.exe infile outfile\n");
        exit(0);
    }

	FILE *fp = fopen(argv[1], "rw");
	remove(argv[2]);
	FILE *fw = fopen(argv[2], "w");
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp,0,SEEK_SET);

	char *string = malloc(fsize+1);
	fread(string, fsize, 1, fp);


	string[fsize] = 0;

	char** entries = malloc(fsize*sizeof(char*));
	unsigned long long *hashArray = malloc(sizeof(unsigned long long)*fsize);
	int i = 0;
	int ent = 0;
	int entstart = 0;
	int entsize = 0;
	while (1) {
		if (i==fsize){
			break;
		}
		if (string[i] != '<'){
			++i;
		}
		else {
			entsize = (i-entstart);
			entries[ent] = malloc(entsize*sizeof(char)+1);
			entries[ent][entsize*sizeof(char)] = 0;
			memcpy(entries[ent], &string[entstart], entsize*sizeof(char));
			hashArray[ent] = hash(entries[ent]);
			int j = 0;
			int tmp = 1;
			for (j = 0; j < ent-1; ++j){
				if (hashArray[ent] == hashArray[j]){
					tmp = 0;
					break;
				}
			}
			if (tmp == 1) {
                fwrite(entries[ent], sizeof(char), strlen(entries[ent]), fw);
			}
			++ent;
			entstart = i;
			++i;
		}
	}
	for(i = 0; i < ent; ++i)
		free(entries[i]);
	free(entries);
	free(string);
	fclose(fp);
	fclose(fw);
	return 0;
}
